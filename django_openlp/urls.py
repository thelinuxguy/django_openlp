from django.conf.urls import url, include
from django.contrib import admin

from rest_framework import routers

from songs import views


router = routers.DefaultRouter()
router.register(r'songs', views.SongViewSet)
router.register(r'songbooks', views.SongBookViewSet)
router.register(r'topics', views.TopicViewSet)
router.register(r'authors', views.AuthorViewSet)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls',
        namespace='rest_framework'))
]
