from django.contrib import admin

from songs.models import (Author, Song, SongBook, Topic, Profile,
                          ConflictingUpdate)

# Register your models here.

admin.site.register(Author)
admin.site.register(Song)
admin.site.register(Topic)
admin.site.register(SongBook)
admin.site.register(Profile)
admin.site.register(ConflictingUpdate)
