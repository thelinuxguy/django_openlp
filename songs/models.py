import uuid

from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from rest_framework.authtoken.models import Token


class BaseModel(models.Model):
    uuid = models.UUIDField(blank=True, unique=True)

    class Meta:
        abstract = True

class Author(BaseModel):
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    display_name = models.CharField(max_length=255)

    def __str__(self):
        return self.display_name


class SongBook(BaseModel):
    name = models.CharField(max_length=128)
    publisher = models.CharField(max_length=128, null=True)

    def __str__(self):
        return self.name

class Topic(BaseModel):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class Song(BaseModel):
    title = models.CharField(max_length=255)
    alternate_title = models.CharField(max_length=255, null=True, blank=True)
    lyrics = models.TextField()
    verse_order = models.CharField(max_length=128, null=True, blank=True)
    copyright = models.CharField(max_length=255, null=True, blank=True)
    comments = models.TextField(null=True, blank=True)
    ccli_number = models.CharField(max_length=64, null=True, blank=True)
    theme_name = models.CharField(max_length=128, null=True, blank=True)
    search_title = models.CharField(max_length=255)
    search_lyrics = models.TextField()
    create_date = models.DateTimeField(null=True)
    last_modified = models.DateTimeField(null=True)

    topics = models.ManyToManyField(Topic, blank=True)
    songbooks = models.ManyToManyField(SongBook, blank=True)


    def __str__(self):
        if self.alternate_title:
            return '{} ({})'.format(self.title, self.alternate_title)
        else:
            return self.title


LATEST_TIMESTAMP = 0
LATEST_VERSION = 1
NO_CONFLICTS = 3
ALWAYS_UPDATE = 4
NEVER_UPDATE = 5
UPDATE_POLICIES = (
        (LATEST_TIMESTAMP, 'Latest Timestamp'),
        (LATEST_VERSION, 'Latest Version'),
        (NO_CONFLICTS, 'Do not allow conflicts'),
        (ALWAYS_UPDATE, 'Always overwrite'),
        (NEVER_UPDATE, 'Do not allow writes'),
        )

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,
                                primary_key=True)
    last_checkin = models.DateTimeField(null=True, blank=True)
    update_policy = models.IntegerField(choices=UPDATE_POLICIES,
                                        default=NEVER_UPDATE)

    def __str__(self):
        return '{}'.format(self.user)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_instance(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
        Profile.objects.create(user=instance)

class ConflictingUpdate(models.Model):
    user = models.ForeignKey(User)
    created = models.DateTimeField(auto_now=True)
    data = models.TextField()
