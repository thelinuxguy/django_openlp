import uuid

from rest_framework import serializers

from songs.models import Author, Song, SongBook, Topic


class OpenLPSerializer(serializers.HyperlinkedModelSerializer):
    uuid = serializers.UUIDField(default=uuid.uuid4, allow_null=True)

    def create(self, validated_data):
        ModelClass = self.Meta.model
        if 'uuid' in validated_data and ModelClass.objects.filter(uuid=validated_data['uuid']).exists():
            instance = ModelClass.objects.get(uuid=validated_data['uuid'])
            return super(OpenLPSerializer, self).update(instance, validated_data)
        else:
            if validated_data['uuid'] is None:
                validated_data['uuid'] = uuid.uuid4()
            return super(OpenLPSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        return super(OpenLPSerializer, self).update(instance, validated_data)


class SongBookSerializer(OpenLPSerializer):
    class Meta:
        model = SongBook


class SongSerializer(OpenLPSerializer):
    class Meta:
        model = Song


class AuthorSerializer(OpenLPSerializer):
    class Meta:
        model = Author


class TopicSerializer(OpenLPSerializer):
    class Meta:
        model = Topic

