from django.shortcuts import render
from django.http import Http404
from django.core.exceptions import PermissionDenied

from rest_framework import generics, viewsets, mixins
from rest_framework.response import Response

import songs.models
from songs.models import Author, Song, SongBook, Topic
from songs.serializers import (AuthorSerializer, SongSerializer,
                               SongBookSerializer, TopicSerializer)


class OpenLPUpdateModelMixin(mixins.UpdateModelMixin):
    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data,
                                         partial=partial)
        serializer.is_valid(raise_exception=True)

        update_policy = request.user.profile.update_policy
        if update_policy == songs.models.LATEST_TIMESTAMP:
            pass
        elif update_policy == songs.models.LATEST_VERSION:
            pass
        elif update_policy == songs.models.NO_CONFLICTS:
            pass
        elif update_policy == songs.models.ALWAYS_UPDATE:
            pass
        elif update_policy == songs.models.NEVER_UPDATE:
            raise PermissionDenied

        self.perform_update(serializer)
        return Response(serializer.data)


class SongBookViewSet(OpenLPUpdateModelMixin, viewsets.ModelViewSet):
    queryset = SongBook.objects.all()
    serializer_class = SongBookSerializer


class SongViewSet(OpenLPUpdateModelMixin, viewsets.ModelViewSet):
    queryset = Song.objects.all()
    serializer_class = SongSerializer


class AuthorViewSet(OpenLPUpdateModelMixin, viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer


class TopicViewSet(OpenLPUpdateModelMixin, viewsets.ModelViewSet):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer
